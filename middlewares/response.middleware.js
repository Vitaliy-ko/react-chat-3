const responseMiddleware = (req, res) => {
  if (res.err) {
    const { status, message } = res.err;
    const response = {
      error: true,
      message
    };
    res.status(status).send(response);
  } else {
    const { status, payload } = res.data;
    res.status(status).send(payload);
  }
};

exports.responseMiddleware = responseMiddleware;
