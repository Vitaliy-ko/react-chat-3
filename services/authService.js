const UserService = require('./userService');
const { UserNotFoundError } = require('./../utils/errors/userErrors');

class AuthService {
  login(userData) {
    const user = UserService.search(userData);
    if (!user) {
      throw new UserNotFoundError('User not found');
    }
    return { payload: user, status: 200 };
  }
}

module.exports = new AuthService();
