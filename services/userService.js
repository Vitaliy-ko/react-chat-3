const { UserRepository } = require('../repositories/userRepository');
const {
  UserConflictError,
  UserNotFoundError
} = require('../utils/errors/userErrors');

class UserService {
  async createUser(user) {
    const isUserExists = this.search(user);
    if (isUserExists) {
      throw new UserConflictError();
    }

    const createdUser = await UserRepository.create(user);
    return this._returnCreatedUserData(createdUser);
  }

  async updateUser(id, dataToUpdate) {
    const user = this.search({ id });

    if (!user) {
      throw new UserNotFoundError();
    }

    const updatedUser = await UserRepository.update(id, dataToUpdate);
    return this._returnUserData(updatedUser);
  }

  async getUserData(id) {
    const user = await this.search({ id });
    if (!user) {
      throw new UserNotFoundError();
    }
    return user;
  }

  async getUser(id) {
    const user = await this.search({ id });
    if (!user) {
      throw new UserNotFoundError();
    }
    return this._returnUserData(user);
  }

  async getUsers() {
    const users = await UserRepository.getAll();
    return this._returnUsersData(users);
  }

  async deleteUser(id) {
    const deletedUser = await UserRepository.delete(id);
    if (!deletedUser.length) {
      throw new UserNotFoundError();
    }
    return this._returnUserData(deletedUser[0]);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  _returnUserData(user) {
    delete user.password;
    return { payload: user, status: 200 };
  }

  _returnUsersData(users) {
    users.forEach(user => {
      delete user.password;
    });
    return { payload: users, status: 200 };
  }

  _returnCreatedUserData(createdUser) {
    delete createdUser.password;
    return { payload: createdUser, status: 200 }; //201
  }
}

module.exports = new UserService();
