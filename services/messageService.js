const { MessageRepository } = require('../repositories/MessageRepository');
const { NotFoundError } = require('./../utils/errors/baseErrors');

class MessageService {
  async addMessage(msg) {
    const newMessage = {
      ...this._messageTemplate,
      ...msg,
    };

    const message = await MessageRepository.create(newMessage);
    return this._returnAddedMessage(message);
  }

  _messageTemplate = {
    text: '',
    user: '',
    avatar: '',
    userId: '',
    editedAt: '',
    like: new Map(),
  };

  async getMessages() {
    const messages = await MessageRepository.getAll();
    return this._returnData(messages);
  }

  async getMessage(id) {
    const message = await this.search({ id });
    if (!message) {
      throw new NotFoundError();
    }
    return this._returnData(message);
  }

  async deleteMessage(id) {
    const deletedMessage = await MessageRepository.delete(id);
    if (!deletedMessage.length) {
      throw new NotFoundError();
    }
    return this._returnData(deletedMessage[0]);
  }

  async updateMessage(id, dataToUpdate) {
    const message = this.search({ id });
    if (!message) {
      throw new NotFoundError();
    }
    const updatedUser = await MessageRepository.update(id, dataToUpdate);
    return this._returnData(updatedUser);
  }

  _returnAddedMessage(message) {
    return { payload: message, status: 201 };
  }

  _returnData(data) {
    return { payload: data, status: 200 };
  }

  search(search) {
    const message = MessageRepository.getOne(search);
    if (!message) {
      return null;
    }
    return message;
  }
}

module.exports = new MessageService();
