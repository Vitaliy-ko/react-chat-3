import React, { useState, useEffect, useCallback } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import {
  getMessages,
  sendMessage,
  editMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  hideModal,
} from './actions';
import { Spinner } from '../../components/Spinner';
import { container } from './styles.module.scss';
import { MessageArea } from '../../components/MessageArea';
import { ChatHeader } from '../../components/ChatHeader';
import { InputMessage } from '../../components/InputMessage';
import MessageEditor from '../../components/MessageEditor';

const Chat = ({
  isMessagesLoaded,
  messages,
  getMessages: getAllMessages,
  sendMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  editingMessage,
  editMessage,
  currentUser
}) => {
  const [messageText, setMessageText] = useState('');
  
  const onUpdateTextMessage = () => {
    const updatedMessage = {
      ...editingMessage,
      text: messageText,
    };
    editMessage(updatedMessage);
  };

  const onUpButton = useCallback(
    (event) => {
      if (event.key !== 'ArrowUp') {
        return;
      }
      setEditingMessage();
    },
    [setEditingMessage]
  );

  useEffect(() => {
    getAllMessages();

    document.addEventListener('keydown', onUpButton);
    return () => {
      document.removeEventListener('keydown', onUpButton);
    };
  }, [getAllMessages, onUpButton]);

  return (
    <div className={container}>
      {!isMessagesLoaded ? (
        <Spinner />
      ) : (
        <>
          <Route exact path="/">
            <ChatHeader messages={messages} />
            <MessageArea
              messages={messages}
              currentUser={currentUser}
              setEditingMessage={setEditingMessage}
              likeMessage={likeMessage}
              deleteMessage={deleteMessage}
            />
            <InputMessage
              sendMessage={sendMessage}
              currentUser={currentUser}
              editingMessage={editingMessage}
              messageText={messageText}
              setMessageText={setMessageText}
            />
          </Route>
          <Route path="/edit-message">
            <MessageEditor
              messageText={messageText}
              setMessageText={setMessageText}
              onUpdateTextMessage={onUpdateTextMessage}
              editingMessage={editingMessage}
            />
          </Route>
        </>
      )}
    </div>
  );
};

const mapStateToProps = (rootState) => ({
  messages: rootState.messages.messages,
  isMessagesLoaded: rootState.messages.isMessagesLoaded,
  editingMessage: rootState.messages.editingMessage,
  isEditModalShown: rootState.messages.isEditModalShown,
  currentUser: rootState.auth.user
});

const actions = {
  getMessages,
  sendMessage,
  editMessage,
  deleteMessage,
  likeMessage,
  setEditingMessage,
  hideModal,
};

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
