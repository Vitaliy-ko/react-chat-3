import {
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR,
  GET_MESSAGES,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_ERROR,
  EDIT_MESSAGE,
  EDIT_MESSAGE_SUCCESS,
  EDIT_MESSAGE_ERROR,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_ERROR,
  LIKE_MESSAGE,
  LIKE_MESSAGE_SUCCESS,
  LIKE_MESSAGE_ERROR,
  SET_EDITING_MESSAGE
} from './actionsTypes';
// import { likeMessage } from './actions';

const initialState = {
  error: null,
  messages: [],
  editingMessage: null,
  isMessagesLoaded: false,
  isMessageSending: false,
  isMessageUpdating: false,
  isMessageDeleting: false,
  isMessageLiking: false,
};

const sendMessageStart = (state) => ({
  ...state,
  isMessageSending: true,
});

const sendMessageSuccess = (state) => ({
  ...state,
  isMessageSending: false,
});

const sendMessageError = (state, error = null) => ({
  ...state,
  error,
});

const setEditingMessage = (state, message) => ({
  ...state,
  editingMessage: message,
});

// ! get
const getMessagesStart = (state) => ({
  ...state,
  isMessagesLoaded: false,
});

const getMessagesSuccess = (state, { messages }) => ({
  ...state,
  isMessagesLoaded: true,
  messages,
});

const getMessagesError = (state, error = null) => ({
  ...state,
  error,
});

// ! update
const editMessageStart = (state) => ({
  ...state,
  isMessageUpdating: true,
  editingMessage: null
});

const editMessageSuccess = (state) => ({
  ...state,
  isMessageUpdating: false,
});

const editMessagesError = (state, error = null) => ({
  ...state,
  error,
});

// ! delete
const deleteMessageStart = (state) => ({
  ...state,
  isMessageDeleting: true,
});

const deleteMessageSuccess = (state) => ({
  ...state,
  isMessageDeleting: false,
});

const deleteMessagesError = (state, error = null) => ({
  ...state,
  error,
});

// ! like
const likeMessageStart = (state) => ({
  ...state,
  isMessageLiking: true,
});

const likeMessageSuccess = (state) => ({
  ...state,
  isMessageLiking: false,
});

const likeMessagesError = (state, error = null) => ({
  ...state,
  error,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE:
      return sendMessageStart(state);
    case SEND_MESSAGE_SUCCESS:
      return sendMessageSuccess(state);
    case SEND_MESSAGE_ERROR:
      return sendMessageError(state);
    //
    case GET_MESSAGES:
      return getMessagesStart(state);
    case GET_MESSAGES_SUCCESS:
      return getMessagesSuccess(state, action.payload);
    case GET_MESSAGES_ERROR:
      return getMessagesError(state, action.payload);
    //
    case SET_EDITING_MESSAGE:
      return setEditingMessage(state, action.payload);
    case EDIT_MESSAGE:
      return editMessageStart(state);
    case EDIT_MESSAGE_SUCCESS:
      return editMessageSuccess(state);
    case EDIT_MESSAGE_ERROR:
      return editMessagesError(state, action);
    //
    case DELETE_MESSAGE:
      return deleteMessageStart(state);
    case DELETE_MESSAGE_SUCCESS:
      return deleteMessageSuccess(state);
    case DELETE_MESSAGE_ERROR:
      return deleteMessagesError(state, action);
    //
    case LIKE_MESSAGE:
      return likeMessageStart(state, action);
    case LIKE_MESSAGE_SUCCESS:
      return likeMessageSuccess(state);
    case LIKE_MESSAGE_ERROR:
      return likeMessagesError(state, action);
    default:
      return state;
  }
};