import {
  GET_MESSAGES_SUCCESS,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  HIDE_MODAL,
  GET_MESSAGES,
  SET_EDITING_MESSAGE,
} from './actionsTypes';

export const sendMessage = (message) => {
  return {
    type: SEND_MESSAGE,
    payload: { message },
  };
};

export const getMessages = () => ({
  type: GET_MESSAGES,
});

export const setEditingMessage = (message) => ({
  type: SET_EDITING_MESSAGE,
  payload: message,
});

export const editMessage = (message) => ({
  type: EDIT_MESSAGE,
  payload: { message },
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: { id },
});

const likeMessageSuccess = (messages) => ({
  type: LIKE_MESSAGE,
  messages,
});

export const hideModal = () => {
  return {
    type: HIDE_MODAL,
  };
};

export const likeMessage = (messageId, currentUserId) => (
  dispatch,
  getState
) => {
  const { messages } = getState().messages;
  const updatedMessages = [...messages];

  const likeMessageIndex = updatedMessages.findIndex(
    (message) => message.id === messageId
  );

  const editingMessage = updatedMessages[likeMessageIndex];

  const isMessageLiked =
    editingMessage.like && editingMessage?.like[currentUserId];

  if (isMessageLiked) {
    delete editingMessage.like[currentUserId];
  } else {
    editingMessage.like = {};
    editingMessage.like[currentUserId] = true;
  }

  updatedMessages.splice(likeMessageIndex, 1, editingMessage);
  dispatch(likeMessageSuccess(updatedMessages));
};

// export const setEditingMessage = (message) => (dispatch, getState) => {
//   let resultMessage = message;

//   if (!message) {
//     const { messages } = getState().messages;
//     resultMessage = messages[messages.length - 1];

//     if (resultMessage.userId !== currentUserId) {
//       return;
//     }
//   }

//   dispatch(setEditingMessageSuccess(resultMessage));
// };
