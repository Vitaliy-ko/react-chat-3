import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  SEND_MESSAGE,
  SEND_MESSAGE_SUCCESS,
  SEND_MESSAGE_ERROR,
  GET_MESSAGES,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_ERROR,
  SET_EDITING_MESSAGE,
  EDIT_MESSAGE,
  EDIT_MESSAGE_SUCCESS,
  EDIT_MESSAGE_ERROR,
  DELETE_MESSAGE,
  DELETE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_ERROR,
  LIKE_MESSAGE,
  LIKE_MESSAGE_SUCCESS,
  LIKE_MESSAGE_ERROR,
} from './actionsTypes';
import {
  get,
  post,
  put as putMethod,
  deleteReq,
} from './../../helpers/requestHelper';

const path = 'message';

export function* sendMessage(action) {
  const { message } = action.payload;
  try {
    yield call(post, `${path}`, message);
    yield put({ type: SEND_MESSAGE_SUCCESS });
    yield put({ type: GET_MESSAGES });
  } catch (error) {
    yield put({ type: SEND_MESSAGE_ERROR });
  }
}

function* watchSendMessage() {
  yield takeEvery(SEND_MESSAGE, sendMessage);
}

export function* getMessages() {
  try {
    const messages = yield call(get, `${path}`);
    yield put({ type: GET_MESSAGES_SUCCESS, payload: { messages } });
  } catch (error) {
    yield put({ type: GET_MESSAGES_ERROR });
  }
}

function* watchGetMessages() {
  yield takeEvery(GET_MESSAGES, getMessages);
}

export function* updateMessage(action) {
  const { message } = action.payload;
  const { id } = message;

  try {
    yield call(putMethod, `${path}`, id, message);
    yield put({ type: EDIT_MESSAGE_SUCCESS });
    yield put({ type: GET_MESSAGES });
  } catch (error) {
    yield put({ type: EDIT_MESSAGE_ERROR });
  }
}

function* watchEditMessages() {
  yield takeEvery(EDIT_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
  const { id } = action.payload;

  try {
    yield call(deleteReq, `${path}`, id);
    yield put({ type: DELETE_MESSAGE_SUCCESS });
    yield put({ type: GET_MESSAGES });
  } catch (error) {
    yield put({ type: DELETE_MESSAGE_ERROR });
  }
}

function* watchDeleteMessages() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export default function* messages() {
  yield all([
    watchSendMessage(),
    watchGetMessages(),
    watchEditMessages(),
    watchDeleteMessages(),
  ]);
}
