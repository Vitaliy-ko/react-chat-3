import { LOGIN, LOAD_USER } from './actionTypes';

export const login = (user) => ({
  type: LOGIN,
  payload: { user },
});

export const loadCurrentUser = () => ({
  type: LOAD_USER
});