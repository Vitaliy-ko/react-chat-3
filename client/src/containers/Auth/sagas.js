import { call, put, takeEvery, all } from 'redux-saga/effects';

import { post } from './../../helpers/requestHelper';
import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR } from './actionTypes';

const path = 'auth';

export function* login(action) {

  const { user } = action.payload;
  try {
    const userData = yield call(post, `${path}`, user);
    console.log('userData', userData);

    if (userData.error) {
      yield put({ type: LOGIN_ERROR, payload: userData });
      return;
    }
    yield put({ type: LOGIN_SUCCESS, payload: { user: userData } });
  } catch (error) {
    yield put({ type: LOGIN_ERROR });
  }
}

function* watchLogin() {
  yield takeEvery(LOGIN, login);
}

export default function* messages() {
  yield all([watchLogin()]);
}
