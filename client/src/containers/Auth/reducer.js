import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR } from './actionTypes';

const initialState = {
  user: {
    id: '',
    name: '',
    isAdmin: false,
  },
  errorMessage: '',
  isError: false,
  isLoginProcessing: false,
  isLoading: true,
  isAuthorized: false,
};

const loginStart = (state) => ({
  ...state,
  isLoginProcessing: true,
});

const loginSuccess = (state, action) => {
  const { user } = action.payload;
  return {
    ...state,
    isLoginProcessing: false,
    user,
    isAuthorized: true,
  };
};

const loginError = (state, action) => {
  const { message } = action.payload;
  return {
    ...state,
    isError: true,
    errorMessage: message,
  };
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return loginStart(state);
    case LOGIN_SUCCESS:
      return loginSuccess(state, action);
    case LOGIN_ERROR:
      return loginError(state, action);
    default:
      return state;
  }
};
