import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Login from './../../components/Login';
import { login } from './actions';
import styles from './styles.module.scss';

const Auth = ({ login: loginUser }) => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  const onSubmit = (event) => {
    event.preventDefault();
    if (!name || !password) {
      return;
    }
    loginUser({ name, password });
  };

  return (
    <div className={styles.container}>
      <form onSubmit={onSubmit} className={styles.auth}>
        <Login
          name={name}
          password={password}
          setName={setName}
          setPassword={setPassword}
        />
      </form>
    </div>
  );
};

const mapStateToProps = (rootState) => ({
  user: rootState.auth.user,
});

const actions = { login };

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
