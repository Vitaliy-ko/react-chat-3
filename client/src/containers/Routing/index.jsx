import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Header from './../../components/Header';
import Spinner from './../../components/Spinner';
import Chat from './../../containers/Chat';
import Auth from './../../containers/Auth';
import styles from './styles.module.scss';
import { Footer } from '../../components/Footer';

const Routing = ({ isLoading, isAuthorized, isAdmin }) => {
  return isLoading ? (
    <Spinner />
  ) : (
    <div className={styles.fill}>
      {isAuthorized && (
        <header>
          <Header isAdmin={isAdmin} />
        </header>
      )}
      <main>
        <Switch>
          {isAuthorized ? (
            <>
              {/* {isAdmin ? (
                  <Route exact component={Users} path="users" />
                  <Route exact component={UserEditor} path="users/:id" />
              ) : (
                ''
              )} */}
              {/* <Route exact component={MessageEditor} path="message-editor/:id" /> */}
              <Route path="/" component={Chat} />
            </>
          ) : (
            <Route exact path="/" component={Auth} />
          )}
          <Redirect to='/'/>
        </Switch>
          {isAuthorized && (
            <header>
              <Footer isAdmin={isAdmin} />
            </header>
          )}
      </main>
    </div>
  );
};

const actions = {};

const mapStateToProps = ({ auth }) => ({
  isAuthorized: auth.isAuthorized,
  // user: auth.user,
  isAdmin: auth.user.isAdmin,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Routing);
