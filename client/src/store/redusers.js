import { combineReducers } from 'redux';

import messagesReducer from './../containers/Chat/reducers';
import authReducer from './../containers/Auth/reducer';

const reducers = {
  messages: messagesReducer,
  auth: authReducer
};

const rootReducer = combineReducers({
  ...reducers,
});

export default rootReducer;
