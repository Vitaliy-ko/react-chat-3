import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import Routing from './containers/Routing';
import './App.scss';



function App() {
  return (
    <div className="App">
      <Routing />
    </div>
  );
}

export default App;
