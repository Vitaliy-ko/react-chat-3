import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const Login = ({ name, password, setName, setPassword }) => {
  return (
    <>
      <h2 className={styles.header}>Login</h2>
      <div className="form-group row">
        <label htmlFor="inputText" className="col-sm-2 col-form-label">
          Name
        </label>
        <div className="col-sm-10">
          <input
            type="text"
            className="form-control"
            id="inputText"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
      </div>
      <div className="form-group row">
        <label htmlFor="inputPassword" className="col-sm-2 col-form-label">
          Password
        </label>
        <div className="col-sm-10">
          <input
            type="password"
            className="form-control"
            id="inputPassword"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
      </div>
      <div className={styles.buttonContainer}>
        <button
          type="submit"
          className={['btn', 'btn-warning', styles.button].join(' ')}
        >
          Submit
        </button>
      </div>
    </>
  );
};

Login.propTypes = {
  name: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  setName: PropTypes.func.isRequired,
  setPassword: PropTypes.func.isRequired,
};

export default Login;
