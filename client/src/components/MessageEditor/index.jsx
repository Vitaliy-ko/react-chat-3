import React from 'react';

import styles from './styles.module.scss';

const MessageEditor = ({
  messageText,
  setMessageText,
  onUpdateTextMessage,
  editingMessage
}) => {
  return (
    <form className={styles.container} onSubmit={onUpdateTextMessage}>
      <h2 className={styles.header}>Edit Message</h2>
      <div className={['form-group', styles.formGroup].join(' ')}>
        <label htmlFor={['messageInput', styles.inputLabel].join(' ')}>
          Message
        </label>
        <textarea
          className={['form-control', styles.messageInput].join(' ')}
          id="messageInput"
          value={messageText || editingMessage.text}
          onChange={(e) => setMessageText(e.target.value)}
        ></textarea>
      </div>
      <div className={styles.buttonContainer}>
        <button
          type="submit"
          className={['btn', 'btn-warning', styles.button].join(' ')}
        >
          Update message
        </button>
      </div>
    </form>
  );
};

export default MessageEditor;
