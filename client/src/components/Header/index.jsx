import React from 'react';

import styles from './styles.module.scss';
import logo from '../../assets/logo/logo.png';
import { Link } from 'react-router-dom';

const Header = ({ isAdmin }) => {
  return (
    <div className={styles.container}>
      <img src={logo} alt="Logo" className={styles.image} />
      {isAdmin ? <Link to="/users">Users</Link> : ''}
    </div>
  );
};

export default Header;
