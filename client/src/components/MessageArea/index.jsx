import React, { useEffect, useRef } from 'react';

import { Message } from '../Message';
import styles from './styles.module.scss';

export const MessageArea = ({
  messages,
  currentUser,
  setEditingMessage,
  setIsEditMessageMode,
  likeMessage,
  deleteMessage,
  setEditingMessageText,
}) => {
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(scrollToBottom, [messages]);

  const oneDaySeconds = 86400 * 1000;
  let lastDate = null;
  let showDate = null;
  
  const allMessages = messages.map((message) => {
    const createdAt = Date.parse(message.createdAt);
    let isMessageSeparatorShown = false;
    if (!lastDate) {
      lastDate = createdAt;
    }

    if (lastDate + oneDaySeconds < createdAt) {
      isMessageSeparatorShown = true;
      lastDate = createdAt;
    }

    if (!isMessageSeparatorShown) {
      showDate = createdAt;
    }

    return (
      <Message
        key={message.id}
        message={message}
        currentUser={currentUser}
        setEditingMessage={setEditingMessage}
        setIsEditMessageMode={setIsEditMessageMode}
        likeMessage={likeMessage}
        deleteMessage={deleteMessage}
        setEditingMessageText={setEditingMessageText}
        isMessageSeparatorShown={isMessageSeparatorShown}
        showDate={showDate}
      />
    );
  });

  return (
    <>
      <div className={styles.container}>
        {allMessages}
        <div ref={messagesEndRef}></div>
      </div>
    </>
  );
};
