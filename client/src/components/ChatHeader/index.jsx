import React from 'react';
import moment from 'moment';

import styles from './styles.module.scss';

const getMessagesData = (messages) => {
  const users = [];
  let lastMessageDate = 0;
  messages.forEach((message) => {
    const { createdAt, user } = message;
    const isUserNameAdded = users.includes(user);
    const dateInSeconds = Date.parse(createdAt);

    if (!isUserNameAdded) {
      users.push(message.user);
    }

    if (lastMessageDate < dateInSeconds) {
      lastMessageDate = dateInSeconds;
    }
  });

  return { users, lastMessageDate };
};

export const ChatHeader = ({ messages }) => {
  const { users, lastMessageDate } = getMessagesData(messages);

  return (
    <div className={styles.header}>
      <div className={styles.container}>
        <p>My chat</p>
        <p>Users count: {users.length}</p>
        <p>Messages count: {messages.length}</p>
        <p>
          Last message:{' '}
          {moment(lastMessageDate).format('MMMM Do YYYY, h:mm:ss a')}
        </p>
      </div>
    </div>
  );
};
