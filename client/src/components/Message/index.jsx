import React, { useState } from 'react';
import moment from 'moment';
import { useHistory } from 'react-router-dom';

import styles from './style.module.scss';

const getMessageInformation = (createdAt, editedAt) => {
  let messageDate = moment(createdAt).format('MMMM Do YYYY, h:mm:ss a');
  if (editedAt) {
    messageDate =
      'Edited: ' + moment(editedAt).format('MMMM Do YYYY, h:mm:ss a');
  }
  return messageDate;
};

// todo исправить проверку id текущего пользователя
export const Message = ({
  message,
  currentUser,
  likeMessage,
  deleteMessage,
  isMessageSeparatorShown,
  showDate,
  setEditingMessage,
}) => {
  const { id, text, user, userId, avatar, createdAt, editedAt } = message;
  const {id: currentUserId} = currentUser;
  const isOwner = currentUserId === userId;
  const isLiked = message.like && message.like[currentUserId];
  const messageDate = getMessageInformation(createdAt, editedAt);
  const [isShown, setIsShown] = useState(false);
  const history = useHistory();

  const editHandler = () => {
    history.push(`/edit-message/${id}`);
    setEditingMessage(message);
  };

  const likeHandler = () => {
    likeMessage(id, currentUserId);
  };

  const deleteHandler = () => {
    deleteMessage(id);
  };

  return (
    <>
      {isMessageSeparatorShown ? (
        <div className={styles.separator}>{moment(showDate).from()}</div>
      ) : (
        ''
      )}
      <div
        onMouseEnter={() => setIsShown(true)}
        onMouseLeave={() => setIsShown(false)}
        className={[styles.container, isOwner ? styles.own : ''].join(' ')}
      >
        <img src={avatar} alt={user + ' avatar'} className={styles.avatar} />
        <div className={styles.content}>
          <div className={styles.wrapper}>
            <p className={styles.name}>{user}</p>
            <p className={styles.text}>{text}</p>
          </div>
          <div className={styles.actions}>
            <div>
              {isOwner && isShown ? (
                <div className={styles.ownMessageActions}>
                  <button className={styles.edit} onClick={editHandler}>
                    Edit
                  </button>
                  <button className={styles.delete} onClick={deleteHandler}>
                    Delete
                  </button>
                </div>
              ) : (
                ''
              )}
              {!isOwner ? (
                <button
                  className={[styles.like, isLiked ? styles.liked : ''].join(
                    ' '
                  )}
                  onClick={likeHandler}
                >
                  Like
                </button>
              ) : (
                ''
              )}
            </div>
            <p className={styles.date}>{messageDate}</p>
          </div>
        </div>
      </div>
    </>
  );
};

// export default withRouter(Message)
