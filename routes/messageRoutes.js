const { Router } = require('express');

const MessageService = require('./../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post(
  '/',
  async (req, res, next) => {
    try {
      const message = await MessageService.addMessage(req.body);
      res.data = message;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/',
  async (req, res, next) => {
    try {
      const messages = await MessageService.getMessages(req.body);
      res.data = messages;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const message = await MessageService.getMessage(id);
      res.data = message;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  '/:id',
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const message = await MessageService.deleteMessage(id);
      res.data = message;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  async (req, res, next) => {
    try {
      const { id } = req.params;
      const { body } = req;

      const updatedMessage = await MessageService.updateMessage(id, body);
      res.data = updatedMessage;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
